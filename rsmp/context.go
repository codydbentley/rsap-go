/* -------------------------------------------------------------------------------------------
 * Copyright (c) RedShift Development LLC
 * Licensed under the MIT license. See License.txt in the project root for license information
 * ---------------------------------------------------------------------------------------- */

package rsmp

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
)

type CtxKey int

const (
	MessageKey    CtxKey = 0
	OutputChanKey CtxKey = 1
)

type ActionContext struct {
	context.Context
	message Message
	out     chan<- Message
}

func (ctx *ActionContext) Value(key interface{}) interface{} {
	switch key {
	case MessageKey:
		return ctx.message
	case OutputChanKey:
		return ctx.out
	}
	return ctx.Context.Value(key)
}

func BindData(raw json.RawMessage, data interface{}) error {
	err := json.Unmarshal(raw, data)
	if err != nil {
		return fmt.Errorf("error unmarshalling in BindData: %v", err)
	}
	return nil
}

func Respond(ctx context.Context) func(interface{}) error {
	out, ok := ctx.Value(OutputChanKey).(chan<- Message)
	if !ok {
		return func(_ interface{}) error {
			return errors.New("error retrieving output channel from ActionContext")
		}
	}
	msg, ok := ctx.Value(MessageKey).(Message)
	if !ok {
		return func(_ interface{}) error {
			return errors.New("error retrieving message from ActionContext")
		}
	}
	return func(data interface{}) error {
		raw, err := json.Marshal(data)
		if err != nil {
			return fmt.Errorf("could not marshall data in event emitter: %v", err)
		}
		res := Message{
			RSMP:   "1.0",
			Action: msg.Action,
			Data:   raw,
			ID:     msg.ID,
		}
		out <- res
		return nil
	}
}

func RespondError(ctx context.Context) func(*ErrorMessage) error {
	out, ok := ctx.Value(OutputChanKey).(chan<- Message)
	if !ok {
		return func(_ *ErrorMessage) error {
			return errors.New("error retrieving output channel from ActionContext")
		}
	}
	msg, ok := ctx.Value(MessageKey).(Message)
	if !ok {
		return func(_ *ErrorMessage) error {
			return errors.New("error retrieving message from ActionContext")
		}
	}
	return func(err *ErrorMessage) error {
		res := Message{
			RSMP:   "1.0",
			Action: msg.Action,
			Error:  err,
			ID:     msg.ID,
		}
		out <- res
		return nil
	}
}

func Emit(ctx context.Context) func(string, interface{}) error {
	out, ok := ctx.Value(OutputChanKey).(chan<- Message)
	if !ok {
		return func(_ string, _ interface{}) error {
			return errors.New("error retrieving output channel from ActionContext")
		}
	}
	return func(event string, data interface{}) error {
		raw, err := json.Marshal(data)
		if err != nil {
			return fmt.Errorf("could not marshall data in event emitter: %v", err)
		}
		msg := Message{
			RSMP:   "1.0",
			Action: event,
			Data:   raw,
		}
		out <- msg
		return nil
	}
}
