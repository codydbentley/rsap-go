/* -------------------------------------------------------------------------------------------
 * Copyright (c) RedShift Development LLC
 * Licensed under the MIT license. See License.txt in the project root for license information
 * ---------------------------------------------------------------------------------------- */

package pipe

import (
	"bufio"
	"io"
)

type Pipe interface {
	In() <-chan []byte
	Out(data []byte) error
	Err(data []byte) error
}

type ChildPipe interface {
	In(data []byte) error
	Out() <-chan []byte
	Err() <-chan []byte
}

type stdPipe struct {
	in  chan []byte
	out *bufio.Writer
	err *bufio.Writer
}

func NewStdPipe(in io.Reader, out io.Writer, err io.Writer) Pipe {
	pipe := stdPipe{
		in:  make(chan []byte),
		out: bufio.NewWriter(out),
		err: bufio.NewWriter(err),
	}
	go func() {
		stdin := bufio.NewReader(in)
		for {
			data, e := stdin.ReadBytes('\n')
			if e != nil {
				break
			}
			pipe.in <- data[:len(data)-1]
		}
	}()
	return &pipe
}

func (p *stdPipe) In() <-chan []byte {
	return p.in
}

func (p *stdPipe) Out(data []byte) error {
	_, err := p.out.Write(data)
	if err != nil {
		return err
	}
	if err = p.out.WriteByte('\n'); err != nil {
		return err
	}
	return p.out.Flush()
}

func (p *stdPipe) Err(data []byte) error {
	_, err := p.err.Write(data)
	if err != nil {
		return err
	}
	if err = p.err.WriteByte('\n'); err != nil {
		return err
	}
	return p.err.Flush()
}
